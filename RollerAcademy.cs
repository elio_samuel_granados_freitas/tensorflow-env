﻿using System.Collections;
using System.Collections.Generic;
using MLAgents;
using UnityEngine;

public class RollerAcademy : Academy { 
    private GameObject[] SpawnPoints;
    private GameObject[] Players;
    //List<int> list = new List<int>(); 
    public override void AcademyReset () {
        Players = GameObject.FindGameObjectsWithTag("Player");
        SpawnPoints = GameObject.FindGameObjectsWithTag("spawn");
        List<int> indexValues = FillList(SpawnPoints.Length);

        foreach (GameObject player in Players)
        {
          Rigidbody2D body = player.GetComponent<Rigidbody2D>();
          Transform tPlayer = player.GetComponent<Transform>();
          body.angularVelocity = 0;
          body.velocity = Vector2.zero;
          int index = GetRandom(indexValues);
          Transform tSpawn = SpawnPoints[index].GetComponent<Transform>();
          tPlayer.position = new Vector2 (tSpawn.position.x, tSpawn.position.y);
          indexValues = eliminateValue(indexValues, index);
        }
    }

    private List<int> FillList(int range)
    {
        List<int> newList = new List<int>();

        for(int i = 0; i < range; i++)
        {
          newList.Add(i);
        }

        return newList;
    }
    
    private int GetRandom(List<int> list)
    {
        if(list.Count == 0){
            return -1; // Maybe you want to refill
        }
        int rand = Random.Range(0, list.Count);
        int value = list[rand];
        return value;
    }

    private List<int> eliminateValue(List<int> list, int value) {
        List<int> newList = new List<int> (list);
        newList.Remove(value);
        return newList;
    }
}