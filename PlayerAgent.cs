﻿using System;
using System.Collections;
using System.Collections.Generic;
using MLAgents;
using UnityEngine;
//using UnityEngine.Random;

public class PlayerAgent : Agent {
    //public Transform Target;
    public Transform[] SpawnPoints;
    public float coneDuration;
    public float flashDuration;
    public float flashCooldown;
    public float tauntDuration;

    private bool collided;
   // private Vector2 LastPosition;
   // private Vector2 LastSignal;
    public float speed = 10;

    public GameObject camRangeObj = null;
    public GameObject tauntObj = null;

    public GameObject _flashed = null;

    private Coroutine _flashRoutine;

    protected Rigidbody2D _body;
    private float _lastFlashTime = -10f;
    void Start () {
        _body = GetComponent<Rigidbody2D>();
    }
    public override void AgentReset () {

        //Reset myself;
        this._body.angularVelocity = 0;
        this._body.velocity = Vector2.zero;

        if (SpawnPoints.Length < 2) {
            //Target.position = new Vector3 (0, 0.5f, 0);
            //this.transform.position = new Vector3 (1, 0.5f, 0);
        } else {
            int index = UnityEngine.Random.Range (0, (SpawnPoints.Length - 1));
            int myIndex = (index + 3) % SpawnPoints.Length;
            //Target.position = new Vector3 (SpawnPoints[index].position.x, SpawnPoints[index].position.y, SpawnPoints[index].position.z);
            this.transform.position = new Vector2 (SpawnPoints[myIndex].position.x, SpawnPoints[myIndex].position.y);
        }
        //LastPosition = this.transform.position;
       // LastSignal = Vector2.zero;
    }

    public bool shoot (Vector3 direction) {
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast (transform.position, transform.TransformDirection (direction), out hit, 8.0f)) {
            Debug.DrawRay (transform.position, transform.TransformDirection (direction) * hit.distance, Color.yellow);
            //Debug.Log("Did Hit");
            if (hit.transform.tag == "Player") {
                //Debug.Log("Hit player");
                return true;
            } else {
                return false;
            }
        } else {
            Debug.DrawRay (transform.position, transform.TransformDirection (direction) * 8.0f, Color.blue);
            //Debug.Log("Did not Hit");
            return false;
        }
    }

    public void CaughtPlayerOnPhoto(PlayerAgent CaughtPlayer) {
       Debug.Log("Caught");
    }

    public override void CollectObservations () {
        //Rigidbody tBody = Target.GetComponent<Rigidbody> ();
        // Target and Agent positions
       // AddVectorObs (Target.position);
        //AddVectorObs (tBody.velocity.x);
       // AddVectorObs (tBody.velocity.z);

        // Agent velocity
        AddVectorObs (this.transform.position);
        AddVectorObs (_body.velocity.x);
        AddVectorObs (_body.velocity.y);
        
        // Obstacle
        GameObject[] objects = GameObject.FindGameObjectsWithTag("wall");
        foreach (GameObject item in objects)
        {
          AddVectorObs(item.transform.position);
        }
    }

    protected void Flash()
    {
        if (IsInvoking("EndFlash"))
            return;

        if (Time.time > _lastFlashTime + flashCooldown)
        {
            _lastFlashTime = Time.time;

            camRangeObj.SetActive(true);
            //onDidFlash.Invoke();
            //GameManager.Instance.PlayerTookPhoto(GetPlayerID());
            Invoke("EndConeSprite", coneDuration);
            SetupEndFlash();
        }
    }

    void EndConeSprite()
    {
        camRangeObj.SetActive(false);
    }

    public void SetupEndFlash()
    {
        Invoke("EndFlash", flashDuration);
    }

    void EndFlash()
    {
        //areaLightObj.SetActive(false);
        //lightConeObj.SetActive(false);
    }

    public void WasFlashed()
    {
        if (_flashRoutine != null)
            StopCoroutine(_flashRoutine);
        _flashRoutine = StartCoroutine(OnPlayerFlashed());
    }

    IEnumerator OnPlayerFlashed()
    {
        _flashed.gameObject.SetActive(true);

        yield return new WaitForSeconds(flashDuration / 2);

        EndFlashed();
    }

    void EndFlashed()
    {
      _flashed.gameObject.SetActive(false);
    }

    protected void Taunt()
    {
        if (IsInvoking("DisableTaunt"))
            return;
        tauntObj.SetActive(true);
        Invoke("DisableTaunt", tauntDuration);
    }

    void DisableTaunt()
    {
        tauntObj.SetActive(false);
    }

    public void ProcessMovement (Vector2 moveVector, float angle) {
        _body.velocity = moveVector * speed * Time.deltaTime;

        // Rotate object with left stick or d-pad.
        if (Mathf.Abs(moveVector.x) > Mathf.Epsilon || Mathf.Abs(moveVector.y) > Mathf.Epsilon)
        {
            _body.velocity += new Vector2(speed * Time.deltaTime * moveVector.x, 0);
            _body.velocity += new Vector2(0, speed * Time.deltaTime * moveVector.y);
        }
    }

    public void processHorizontalMovement(float[] vectorAction) {
        if(vectorAction[0] == 1 && vectorAction[1] == 0) {
          //Debug.Log("right");
          ProcessMovement(new Vector2(1.0f , 0.0f), 90);
          camRangeObj.transform.eulerAngles = new Vector3(0, 0, 0);
        } else if(vectorAction[0] == -1 && vectorAction[1] == 0) {
          //Debug.Log("left");
          ProcessMovement(new Vector2(-1.0f, 0.0f), 270);
          camRangeObj.transform.eulerAngles = new Vector3(0, 0, 180);
        }
    }

    public void processVerticalMovement(float[] vectorAction) {
        if(vectorAction[1] == 1 && vectorAction[0] == 0) {
          ProcessMovement(new Vector2(0.0f , 1.0f), 0);
          camRangeObj.transform.eulerAngles = new Vector3(0, 0, 90);
        } else if(vectorAction[1] == -1 && vectorAction[0] == 0) {
          ProcessMovement(new Vector2(0.0f, -1.0f), 180);
          camRangeObj.transform.eulerAngles = new Vector3(0, 0, 270);
        }
    }

    public void processWesternMovement(float[] vectorAction) {
        if(vectorAction[1] == 1 && vectorAction[0] == -1) {
          ProcessMovement(new Vector2(-1.0f , 1.0f), 135);
          camRangeObj.transform.eulerAngles = new Vector3(0, 0, 135);
        } else if(vectorAction[1] == -1 && vectorAction[0] == -1) {
          ProcessMovement(new Vector2(-1.0f, -1.0f), 225);
          camRangeObj.transform.eulerAngles = new Vector3(0, 0, 225);
        }
    }

    public void processEasternMovement(float[] vectorAction) {
        if(vectorAction[1] == 1 && vectorAction[0] == 1) {
          ProcessMovement(new Vector2(1.0f , 1.0f), 45);
          camRangeObj.transform.eulerAngles = new Vector3(0, 0, 45);
        } else if(vectorAction[1] == -1 && vectorAction[0] == 1) {
          ProcessMovement(new Vector2(1.0f, -1.0f), 315);
          camRangeObj.transform.eulerAngles = new Vector3(0, 0, 315);
        }
    }

    public void ActionToInput(float[] vectorAction) {

        if(vectorAction[2] == 1) {
          Flash();
        }
        
        if(vectorAction[3] == 1) {
          Taunt();
        }
        //Debug.Log("X:" + vectorAction[0] + "Y:" + vectorAction[0]);
        if(vectorAction[0] == 0 && vectorAction[1] == 0) {
          ProcessMovement(Vector2.zero, 0);
          return;
        }
        processHorizontalMovement(vectorAction);
        processVerticalMovement(vectorAction);
        processWesternMovement(vectorAction);
        processEasternMovement(vectorAction);

    }

    public override void AgentAction (float[] vectorAction, string textAction) {
        ActionToInput(vectorAction);
       /*Vector3 controlSignal = Vector3.zero;
        controlSignal.x = vectorAction[0];
        controlSignal.z = vectorAction[1];
        rBody.AddForce (controlSignal * speed);
        if (controlSignal.sqrMagnitude > 0) {
            LastSignal = controlSignal;
        }*/

        /* if (vectorAction[2] > 0) {
            if (this.shoot (LastSignal.normalized)) {
                SetReward (1.0f);
                Done ();
            } else {
                AddReward (-0.00005f);
            }
        }*/

        AddReward (-0.00001f);
        //Punish for not moving
       /*float distanceFromMyLastPosition = Vector3.Distance (this.transform.position,
            LastPosition);

        if (distanceFromMyLastPosition < 0.01f) {
            AddReward (-0.0001f);
        }

        LastPosition = this.transform.position;*/

        // Rewards
        //float distanceToTarget = Vector3.Distance (this.transform.position,
         //   Target.position);

        // Reached target
      /*  if (distanceToTarget < 1) {
            SetReward (1.0f);
            Done ();
            //  Debug.Log("Done");
        } */

        // Fell off platform
        if (this.transform.position.y < 0) {
           // Done ();
           // SetReward (-1.0f);
        }

    }
}