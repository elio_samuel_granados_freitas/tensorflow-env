﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CooldownCounter : MonoBehaviour
{
    // Start is called before the first frame update
    private Text text;
    private PlayerController teacherController;
    void Start()
    {
        text = GetComponent<Text> ();
        GameObject teacher = GameObject.Find("Teacher");
        teacherController = teacher.GetComponent<PlayerController> ();
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
      text.text = "CD: " + teacherController.getFlashCooldown();
    }
}
