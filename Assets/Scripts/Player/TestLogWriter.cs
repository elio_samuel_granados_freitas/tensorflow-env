using System.IO;
using System;
using System.Collections.Generic;

class TestLogWriter 
{
    public static void writeLines(List<string> lines) 
    {
        // Create an instance of StreamWriter to write text to a file.
        // The using statement also closes the StreamWriter.
        using (StreamWriter sw = new StreamWriter("logFile.txt")) 
        {
            foreach (string line in lines)
            {
              sw.WriteLine(line);
            }
        }
    }
}