﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AgentProperties {
    public float px;
    public float py;
    public float vx;
    public float vy;
    public string visible;
    public float flashCooldown;

    public AgentProperties (Vector2 Position, Vector2 Velocity, bool isVisible, float flashCooldown) {
        px = Position.x;
        py = Position.y;
        vx = Velocity.x;
        vy = Velocity.y;
        visible = isVisible ? "true" : "false";
        this.flashCooldown = flashCooldown;
    }

    public Vector2 getPosition () {
        return new Vector2 (px, py);
    }

    public Vector2 getVelocity () {
        return new Vector2 (vx, vy);
    }

    public bool getIsVisible() {
        return string.Compare(this.visible, "true") == 0;
    }

    public float getFlashCooldown() {
        return flashCooldown;
    }
}