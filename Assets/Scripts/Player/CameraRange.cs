﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRange : MonoBehaviour {

	private PlayerController _parent;

	void OnTriggerEnter2D(Collider2D other) {
		PlayerController caughtPlayer = other.GetComponent<PlayerController> ();
		if (caughtPlayer != null && caughtPlayer != _parent) {
			_parent.CaughtPlayerOnPhoto(caughtPlayer);
		}
	}

	public void setParent(PlayerController agent) {
		_parent = agent;
	}
}
