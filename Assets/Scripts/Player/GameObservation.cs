﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameObservation
{
    public Observations[] gameObs;

    public GameObservation(Observations[] gameObs) {
        this.gameObs = gameObs;
    }

    public static string stringify(GameObservation GameO)
    {
        return JsonUtility.ToJson(GameO);
    }

    public static void saveJson(string json, string fileName) {
        if (File.Exists(fileName))
        {
            Debug.Log(fileName+" already exists.");
            return;
        }
        var sr = File.CreateText(fileName);
        sr.WriteLine (json);
        sr.Close();
    }

    public static GameObservation loadJson(string fileName) {
        if (File.Exists(fileName))
        {
            //Read the text from directly from the test.txt file
            StreamReader reader = new StreamReader(fileName); 
            String jsonString = reader.ReadToEnd();
            GameObservation result = JsonUtility.FromJson<GameObservation>(jsonString);
            reader.Close();
            return result;
        }
        return null;
    }
}
