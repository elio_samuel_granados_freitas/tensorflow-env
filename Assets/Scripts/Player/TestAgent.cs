﻿using System.Collections;
using System.Collections.Generic;
using MLAgents;
using UnityEngine;

public class TestAgent : Agent {
    public RollerAcademy academy;

    public GameObservation game = null;

    private float maxHorizontalPosition = 9.348099f;
    private float minHorizontalPosition = -9.471906f;
    private float maxVerticalPosition = 4.507236f;
    private float minVerticalPosition = -3.62158f;
    private float maxVelocity = 4f;
    private float minVelocity = -4f;

    private int observationIndex = 0;

    private int match = 0;
    private int mismatch = 0;

    private struct ConfusionMatrix {
        public float tp;
        public float fp;
        public float tn;
        public float fn;
        public float sensitivity;
        public float specificity;
        public float accuracy;
        public float precision;
        public float recall;
        public float f1Score;

        public ConfusionMatrix (bool trash) {
            tp = 0.0f;
            fp = 0.0f;
            tn = 0.0f;
            fn = 0.0f;
            sensitivity = 0.0f;
            specificity = 0.0f;
            accuracy = 0.0f;
            precision = 0.0f;
            recall = 0.0f;
            f1Score = 0.0f;
        }

    }

    private struct MultiConfusionMatrix {
        public float AA;
        public float AB;
        public float AC;

        public float BA;
        public float BB;
        public float BC;

        public float CA;
        public float CB;
        public float CC;

        public float ASensitivity;

        public float APrecision;
        public float AF1Score;

        public float BSensitivity;

        public float BPrecision;
        public float BF1Score;

        public float CSensitivity;

        public float CPrecision;
        public float CF1Score;

        public float accuracy;

        public float meanPrecision;
        public float meanSensitivity;
        public float meanF1Score;

        public MultiConfusionMatrix (bool trash) {
            AA = 0.0f;
            AB = 0.0f;
            AC = 0.0f;

            BA = 0.0f;
            BB = 0.0f;
            BC = 0.0f;

            CA = 0.0f;
            CB = 0.0f;
            CC = 0.0f;

            ASensitivity = 0.0f;
            APrecision = 0.0f;
            AF1Score = 0.0f;

            BSensitivity = 0.0f;
            BPrecision = 0.0f;
            BF1Score = 0.0f;

            CSensitivity = 0.0f;
            CPrecision = 0.0f;
            CF1Score = 0.0f;

            meanPrecision = 0.0f;
            meanSensitivity = 0.0f;
            meanF1Score = 0.0f;
            accuracy = 0.0f;
        }

    }

    private struct ConfusionMatrixGroup {
        public MultiConfusionMatrix verticalNavigationMatrix;
        public MultiConfusionMatrix horizontalNavigationMatrix;

        public ConfusionMatrix flashMatrix;
        public ConfusionMatrix tauntMatrix;

        public ConfusionMatrixGroup (bool trash) {
            verticalNavigationMatrix = new MultiConfusionMatrix (true);
            horizontalNavigationMatrix = new MultiConfusionMatrix (true);
            flashMatrix = new ConfusionMatrix (true);
            tauntMatrix = new ConfusionMatrix (true);
        }
    }

    ConfusionMatrixGroup matrixGroup;

    void Start () { }

    public override void AgentReset () {
        observationIndex = 0;
        match = 0;
        mismatch = 0;
        matrixGroup = new ConfusionMatrixGroup (true);
        game = GameObservation.loadJson ("GameObservation.json");
    }

    public Vector2 normalizePositionVector (Vector2 position) {
        float x = (position.x - minHorizontalPosition) / (maxHorizontalPosition - minHorizontalPosition);
        float y = (position.y - minVerticalPosition) / (maxVerticalPosition - minVerticalPosition);
        return new Vector2 (x, y);
    }

    public Vector2 normalizeVelocityVector (Vector2 velocity) {
        float x = (velocity.x - minVelocity) / (maxVelocity - minVelocity);
        float y = (velocity.y - minVelocity) / (maxVelocity - minVelocity);
        return new Vector2 (x, y);
    }

    private void fillRemaining (int definedTargets) {
        for (int i = 0; i < 4 - definedTargets; i++) {
            Vector2 targetP = new Vector2 (-1, -1);
            Vector2 targetV = new Vector2 (-1, -1);
            AddVectorObs (targetP);
            AddVectorObs (targetV);
            AddVectorObs (false);
            AddVectorObs (0.0f);
        }
    }

    public override void CollectObservations () {

        Observations obs = game.gameObs[observationIndex];
        AgentProperties[] targets = obs.getTargets ();
        int definedTargets = 0;
        // Targets
        foreach (AgentProperties target in targets) {
            if (definedTargets == 3) {
                break;
            }
            AddVectorObs (target.getPosition ());
            AddVectorObs (target.getVelocity ());
            AddVectorObs (target.getIsVisible ());
            AddVectorObs (target.getFlashCooldown ());
            //Debug.Log("target data: " + target.getPosition () + "," + target.getVelocity () + "," + target.getIsVisible () + "," + target.getFlashCooldown () );
            definedTargets++;
        }
        //Debug.Log("-----------------------------");

        if (definedTargets < 3) {
            fillRemaining (definedTargets);
        }

        // Agent
        AgentProperties agent = obs.getPlayer ();
        AddVectorObs (agent.getPosition ());
        AddVectorObs (agent.getVelocity ());
        AddVectorObs (agent.getIsVisible ());
        AddVectorObs (agent.getFlashCooldown ());
        // Debug.Log("agent data: " + agent.getPosition () + "," + agent.getVelocity () + "," + agent.getIsVisible () + "," + agent.getFlashCooldown () );

        // Obstacle
        AddVectorObs (obs.getLastObstacle ());
        AddVectorObs (obs.getObstacleHitTimeStamp ());
        //Debug.Log("obstacle data: " + obs.getLastObstacle () + "," + obs.getObstacleHitTimeStamp ());

    }

    public override void AgentAction (float[] vectorAction, string textAction) {
        //Debug.Log("Action");
        // compareAndAccumulate(vectorAction, game.gameObs[observationIndex].getVectorActions());
        matrixGroup = calculateConfusionMatrixGroups (matrixGroup, vectorAction, game.gameObs[observationIndex].getVectorActions ());
        observationIndex++;
        Debug.Log ("Observation: " + observationIndex + "of " + game.gameObs.Length);
        if (game.gameObs.Length == observationIndex) {
            matrixGroup = calculateParameters (matrixGroup);
            //createResume ();
            generateLogResume (matrixGroup);
            Done ();
            academy.reset ();
        }
    }

    private void generateLogResume (ConfusionMatrixGroup storage) {
        List<string> lines = new List<string>();
        Debug.Log ("*-------------------------------------------------------------------*");
        Debug.Log ("*--------------------------FLASH MATRIX-----------------------------*");
        Debug.Log ("TP:" + storage.flashMatrix.tp);
        Debug.Log ("FP: " + storage.flashMatrix.fp);
        Debug.Log ("TN: " + storage.flashMatrix.tn);
        Debug.Log ("FN: " + storage.flashMatrix.fn);
        lines.Add("*----FLASH MATRIX------");
        lines.Add("TP:" + storage.flashMatrix.tp);
        lines.Add("FP: " + storage.flashMatrix.fp);
        lines.Add("TN: " + storage.flashMatrix.tn);
        lines.Add("FN: " + storage.flashMatrix.fn);
        Debug.Log ("*---------------------------F MATRIX PARAMETERS--------------------------------*");
        Debug.Log ("Sensitivity: " + storage.flashMatrix.sensitivity);
        Debug.Log ("Specificity: " + storage.flashMatrix.specificity);
        Debug.Log ("Accuracy: " + storage.flashMatrix.accuracy);
        Debug.Log ("Precision: " + storage.flashMatrix.precision);
        Debug.Log ("F1Score: " + storage.flashMatrix.f1Score);
        lines.Add("*----FLASH MATRIX PARAMETERS------");
        lines.Add("Sensitivity: " + storage.flashMatrix.sensitivity);
        lines.Add("Specificity: " + storage.flashMatrix.specificity);
        lines.Add("Accuracy: " + storage.flashMatrix.accuracy);
        lines.Add("Precision: " + storage.flashMatrix.precision);
        lines.Add("F1Score: " + storage.flashMatrix.f1Score);
        Debug.Log ("*-------------------------------------------------------------------*");
        Debug.Log ("*-------------------------------------------------------------------*");
        Debug.Log ("*-------------------------------------------------------------------*");
        Debug.Log ("*--------------------------TAUNT MATRIX-----------------------------*");
        Debug.Log ("TP:" + storage.tauntMatrix.tp);
        Debug.Log ("FP: " + storage.tauntMatrix.fp);
        Debug.Log ("TN: " + storage.tauntMatrix.tn);
        Debug.Log ("FN: " + storage.tauntMatrix.fn);
        lines.Add("*----TAUNT MATRIX------");
        lines.Add("TP:" + storage.tauntMatrix.tp);
        lines.Add("FP: " + storage.tauntMatrix.fp);
        lines.Add("TN: " + storage.tauntMatrix.tn);
        lines.Add("FN: " + storage.tauntMatrix.fn);
        Debug.Log ("*---------------------------TAUNT MATRIX PARAMETERS--------------------------------*");
        Debug.Log ("Sensitivity: " + storage.tauntMatrix.sensitivity);
        Debug.Log ("Specificity: " + storage.tauntMatrix.specificity);
        Debug.Log ("Accuracy: " + storage.tauntMatrix.accuracy);
        Debug.Log ("Precision: " + storage.tauntMatrix.precision);
        Debug.Log ("F1Score: " + storage.tauntMatrix.f1Score);
        lines.Add("*----TAUNT MATRIX PARAMETERS------");
        lines.Add("Sensitivity: " + storage.tauntMatrix.sensitivity);
        lines.Add("Specificity: " + storage.tauntMatrix.specificity);
        lines.Add("Accuracy: " + storage.tauntMatrix.accuracy);
        lines.Add("Precision: " + storage.tauntMatrix.precision);
        lines.Add("F1Score: " + storage.tauntMatrix.f1Score);
        Debug.Log ("*-------------------------------------------------------------------*");
        Debug.Log ("*-------------------------------------------------------------------*");
        Debug.Log ("*-------------------------------------------------------------------*");
        Debug.Log ("*--------------------------HORIZONTAL MULTI MATRIX------------------*");
        Debug.Log ("*--------------------------A=Neutral,B=Right,C=Left-----------------*");
        Debug.Log ("AA:" + storage.horizontalNavigationMatrix.AA);
        Debug.Log ("BB: " + storage.horizontalNavigationMatrix.BB);
        Debug.Log ("CC: " + storage.horizontalNavigationMatrix.CC);
        Debug.Log ("AC:" + storage.horizontalNavigationMatrix.AC);
        Debug.Log ("AB: " + storage.horizontalNavigationMatrix.AB);
        Debug.Log ("BC: " + storage.horizontalNavigationMatrix.BC);
        Debug.Log ("BA:" + storage.horizontalNavigationMatrix.BA);
        Debug.Log ("CB: " + storage.horizontalNavigationMatrix.CB);
        Debug.Log ("CA: " + storage.horizontalNavigationMatrix.CA);
        lines.Add("*----HORIZONTAL MATRIX: A=Neutral,B=Right,C=Left------");
        lines.Add("AA:" + storage.horizontalNavigationMatrix.AA);
        lines.Add("BB: " + storage.horizontalNavigationMatrix.BB);
        lines.Add("CC: " + storage.horizontalNavigationMatrix.CC);
        lines.Add("AC:" + storage.horizontalNavigationMatrix.AC);
        lines.Add("AB: " + storage.horizontalNavigationMatrix.AB);
        lines.Add("BC: " + storage.horizontalNavigationMatrix.BC);
        lines.Add("BA:" + storage.horizontalNavigationMatrix.BA);
        lines.Add("CB: " + storage.horizontalNavigationMatrix.CB);
        lines.Add("CA: " + storage.horizontalNavigationMatrix.CA);
        Debug.Log ("*---------------------------Horizontal PARAMETERS--------------------------------*");
        Debug.Log ("mean Sensitivity: " + storage.horizontalNavigationMatrix.meanSensitivity);
        Debug.Log ("mean Precision: " + storage.horizontalNavigationMatrix.meanPrecision);
        Debug.Log ("mean F1Score: " + storage.horizontalNavigationMatrix.meanF1Score);
        Debug.Log ("mean Accuracy: " + storage.horizontalNavigationMatrix.accuracy);
        lines.Add("*----Horizontal PARAMETERS------");
        lines.Add("mean Sensitivity: " + storage.horizontalNavigationMatrix.meanSensitivity);
        lines.Add("mean Precision: " + storage.horizontalNavigationMatrix.meanPrecision);
        lines.Add("mean F1Score: " + storage.horizontalNavigationMatrix.meanF1Score);
        lines.Add("mean Accuracy: " + storage.horizontalNavigationMatrix.accuracy);
        Debug.Log ("*-------------------------------------------------------------------*");
        Debug.Log ("*-------------------------------------------------------------------*");
        Debug.Log ("*-------------------------------------------------------------------*");
        Debug.Log ("*--------------------------VERTICAL MULTI MATRIX------------------*");
        Debug.Log ("*--------------------------A=Neutral,B=Up,C=Down-----------------*");
        Debug.Log ("AA:" + storage.verticalNavigationMatrix.AA);
        Debug.Log ("BB: " + storage.verticalNavigationMatrix.BB);
        Debug.Log ("CC: " + storage.verticalNavigationMatrix.CC);
        Debug.Log ("AC:" + storage.verticalNavigationMatrix.AC);
        Debug.Log ("AB: " + storage.verticalNavigationMatrix.AB);
        Debug.Log ("BC: " + storage.verticalNavigationMatrix.BC);
        Debug.Log ("BA:" + storage.verticalNavigationMatrix.BA);
        Debug.Log ("CB: " + storage.verticalNavigationMatrix.CB);
        Debug.Log ("CA: " + storage.verticalNavigationMatrix.CA);
        lines.Add("*----VERTICAL MATRIX: A=Neutral,B=Up,C=Down------");
        lines.Add("AA:" + storage.verticalNavigationMatrix.AA);
        lines.Add("BB: " + storage.verticalNavigationMatrix.BB);
        lines.Add("CC: " + storage.verticalNavigationMatrix.CC);
        lines.Add("AC:" + storage.verticalNavigationMatrix.AC);
        lines.Add("AB: " + storage.verticalNavigationMatrix.AB);
        lines.Add("BC: " + storage.verticalNavigationMatrix.BC);
        lines.Add("BA:" + storage.verticalNavigationMatrix.BA);
        lines.Add("CB: " + storage.verticalNavigationMatrix.CB);
        lines.Add("CA: " + storage.verticalNavigationMatrix.CA);
        Debug.Log ("*---------------------------Vertical PARAMETERS--------------------------------*");
        Debug.Log ("mean Sensitivity: " + storage.verticalNavigationMatrix.meanSensitivity);
        Debug.Log ("mean Precision: " + storage.verticalNavigationMatrix.meanPrecision);
        Debug.Log ("mean F1Score: " + storage.verticalNavigationMatrix.meanF1Score);
        Debug.Log ("mean Accuracy: " + storage.verticalNavigationMatrix.accuracy);
        lines.Add("*----Vertical PARAMETERS------");
        lines.Add("mean Sensitivity: " + storage.verticalNavigationMatrix.meanSensitivity);
        lines.Add("mean Precision: " + storage.verticalNavigationMatrix.meanPrecision);
        lines.Add("mean F1Score: " + storage.verticalNavigationMatrix.meanF1Score);
        lines.Add("mean Accuracy: " + storage.verticalNavigationMatrix.accuracy);
        Debug.Log ("*-------------------------------------------------------------------*");
        Debug.Log ("*-------------------------------------------------------------------*");
        TestLogWriter.writeLines(lines);
    }

    private ConfusionMatrixGroup calculateParameters (ConfusionMatrixGroup storage) {
        int groups = 8;
        storage.horizontalNavigationMatrix = calculateMatrixParameters(storage.horizontalNavigationMatrix);
        storage.verticalNavigationMatrix = calculateMatrixParameters(storage.verticalNavigationMatrix);
        storage.flashMatrix = calculateMatrixParameters (storage.flashMatrix);
        storage.tauntMatrix = calculateMatrixParameters (storage.tauntMatrix);
        return storage;
    }

    private MultiConfusionMatrix calculateMatrixParameters (MultiConfusionMatrix storage) {
        storage.APrecision = storage.AA / (storage.AA + storage.AB + storage.AC);
        storage.BPrecision = storage.BB / (storage.BA + storage.BB + storage.BC);
        storage.CPrecision = storage.CC / (storage.CA + storage.CB + storage.CC);
        storage.ASensitivity = storage.AA / (storage.AA + storage.BA + storage.CA);
        storage.BSensitivity = storage.BB / (storage.AB + storage.BB + storage.CB);
        storage.CSensitivity = storage.CC / (storage.AC + storage.BC + storage.CC);

        storage.AF1Score = (2 * storage.APrecision * storage.ASensitivity) / (storage.APrecision + storage.ASensitivity);
        storage.BF1Score = (2 * storage.BPrecision * storage.BSensitivity) / (storage.BPrecision + storage.BSensitivity);
        storage.CF1Score = (2 * storage.CPrecision * storage.CSensitivity) / (storage.CPrecision + storage.CSensitivity);

        storage.meanF1Score = (storage.AF1Score + storage.BF1Score + storage.CF1Score) / 3;
        storage.meanPrecision = (storage.APrecision + storage.BPrecision + storage.CPrecision) / 3;
        storage.meanSensitivity = (storage.ASensitivity + storage.BSensitivity + storage.CSensitivity) / 3;

        storage.accuracy = (storage.AA + storage.BB + storage.CC) /(storage.AA + storage.AB + storage.AC + storage.BA + storage.BB + storage.BC + storage.CA + storage.CB + storage.CC);

        return storage;
    }

    private ConfusionMatrix calculateMatrixParameters (ConfusionMatrix storage) {
        storage.specificity = (storage.tn) / (storage.tn + storage.fp);
        storage.precision = (storage.tp) / (storage.tp + storage.fp);
        storage.sensitivity = (storage.tp) / (storage.tp + storage.fn);
        storage.accuracy = (storage.tp + storage.tn) / (storage.tn + storage.fp + storage.tp + storage.fn);
        storage.f1Score = (2 * storage.tp) / (2 * storage.tp + storage.fp + storage.fn);
        return storage;
    }

    private ConfusionMatrixGroup calculateConfusionMatrixGroups (ConfusionMatrixGroup storage, float[] vectorAction, float[] recordedActions) {
        storage.horizontalNavigationMatrix = calculateMultiConfusionMatrix(vectorAction, recordedActions, storage.horizontalNavigationMatrix, 0);
        storage.verticalNavigationMatrix = calculateMultiConfusionMatrix(vectorAction, recordedActions, storage.verticalNavigationMatrix, 1);
        storage.flashMatrix = calculateConfusionMatrix (vectorAction, recordedActions, storage.flashMatrix, 2, 1);
        storage.tauntMatrix = calculateConfusionMatrix (vectorAction, recordedActions, storage.tauntMatrix, 3, 1);
        return storage;
    }

    private MultiConfusionMatrix calculateMultiConfusionMatrix (float[] vectorAction, float[] recordedActions, MultiConfusionMatrix storage, int indexToCheck) {
        /*  A=0 Neutral
         *  B=1 Right /Up
         *  C=2 Left  /Down
         */
        if (recordedActions[indexToCheck] == 0) {
            if (vectorAction[indexToCheck] == 0) {
                storage.AA = storage.AA + 1;
            }
            if (vectorAction[indexToCheck] == 1) {
                storage.AB = storage.AB + 1;
            }
            if (vectorAction[indexToCheck] == 2) {
                storage.AC = storage.AC + 1;
            }
        }
        if (recordedActions[indexToCheck] == 1) {
            if (vectorAction[indexToCheck] == 0) {
                storage.BA = storage.BA + 1;
            }
            if (vectorAction[indexToCheck] == 1) {
                storage.BB = storage.BB + 1;
            }
            if (vectorAction[indexToCheck] == 2) {
                storage.BC = storage.BC + 1;
            }
        }
        if (recordedActions[indexToCheck] == 2) {
            if (vectorAction[indexToCheck] == 0) {
                storage.CA = storage.CA + 1;
            }
            if (vectorAction[indexToCheck] == 1) {
                storage.CB = storage.CB + 1;
            }
            if (vectorAction[indexToCheck] == 2) {
                storage.CC = storage.CC + 1;
            }
        }
        return storage;
    }

    private ConfusionMatrix calculateConfusionMatrix (float[] vectorAction, float[] recordedActions, ConfusionMatrix storage, int indexToCheck, int positiveValue) {
        bool predictedValue = vectorAction[indexToCheck] == positiveValue;
        bool recordedValue = recordedActions[indexToCheck] == positiveValue;
        if (predictedValue == true && recordedValue == true) {
            storage.tp = storage.tp + 1;
        }
        if (predictedValue == true && recordedValue == false) {
            storage.fn = storage.fn + 1;
        }
        if (predictedValue == false && recordedValue == true) {
            storage.fp = storage.fp + 1;
        }
        if (predictedValue == false && recordedValue == false) {
            storage.tn = storage.tn + 1;
        }
        return storage;
    }

    //********************************************** */
    public void createResume () {
        float total = match + mismatch;
        float goodPercentage = (match * 100.0f) / total;
        float badPercentage = (mismatch * 100.0f) / total;
        Debug.Log ("*-------------------------------------------------------------------*");
        Debug.Log ("Good %: " + goodPercentage);
        Debug.Log ("Bad %: " + badPercentage);
        Debug.Log ("Match: " + match);
        Debug.Log ("Mismatch: " + mismatch);
        Debug.Log ("Total: " + total);
    }

    public void compareAndAccumulate (float[] vectorAction, float[] recordedActions) {
        //Debug.Log("Match: " + match);
        //Debug.Log("Mismatch: " + mismatch);
        if (vectorAction[0] == recordedActions[0]) {
            match++;
        } else {
            mismatch++;
        }
        if (vectorAction[1] == recordedActions[1]) {
            match++;
        } else {
            mismatch++;
        }
        if (vectorAction[2] == recordedActions[2]) {
            match++;
        } else {
            mismatch++;
        }
        if (vectorAction[3] == recordedActions[3]) {
            match++;
        } else {
            mismatch++;
        }
        //Debug.Log("Match: " + match);
        //Debug.Log("Mismatch: " + mismatch);
    }
}