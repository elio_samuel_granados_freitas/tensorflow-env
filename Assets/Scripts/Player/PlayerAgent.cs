﻿using System;
using System.Collections;
using System.Collections.Generic;
using MLAgents;
using UnityEngine;

public class PlayerAgent : Agent {

  public RollerAcademy academy;
  public PlayerController controller;

  public bool recordObservations;

  public GameObservation gameObs = null; 

  private float maxHorizontalPosition = 9.348099f;
  private float minHorizontalPosition = -9.471906f;
  private float maxVerticalPosition = 4.507236f;
  private float minVerticalPosition = -3.62158f;
  private float maxVelocity = 4f;
  private float minVelocity = -4f;

  private List<Observations> obsList = new List<Observations>();

  void Start () {
  }

  public override void AgentReset () {
    controller.reset();
  }

  public Vector2 normalizePositionVector(Vector2 position) {
     float x = (position.x - minHorizontalPosition)/(maxHorizontalPosition - minHorizontalPosition);
     float y = (position.y - minVerticalPosition)/(maxVerticalPosition - minVerticalPosition);
     return new Vector2(x , y);
  }

  public Vector2 normalizeVelocityVector(Vector2 velocity) {
     float x = (velocity.x - minVelocity)/(maxVelocity - minVelocity);
     float y = (velocity.y - minVelocity)/(maxVelocity - minVelocity);
     return new Vector2(x , y);
  }

  private AgentProperties[] fillRemaining(int definedTargets, AgentProperties[] targetsObservations) {
      for (int i = 0; i < 4 - definedTargets; i++)
      {
        Vector2 targetP = new Vector2(-1,-1);
        Vector2 targetV = new Vector2(-1,-1);
        AddVectorObs (targetP);
        AddVectorObs (targetV);
        AddVectorObs (false);
        AddVectorObs (0.0f);
        if(recordObservations) {
          targetsObservations[i + definedTargets] = new AgentProperties(targetP, targetV, false, 0.0f);
        }
      }
      return targetsObservations;
  }

  public override void CollectObservations () {
    GameObject[] targets = controller.getAlltargets ();
    int definedTargets = 0;
    AgentProperties[] targetsObservations = new AgentProperties[4];
    int observationIndex = 0;
    
    // Targets
    foreach (GameObject target in targets) {
      Rigidbody2D tBody = target.GetComponent<Rigidbody2D> ();
      PlayerController agentController = target.GetComponent<PlayerController> ();
      Vector2 targetP = normalizePositionVector(target.transform.position);
      Vector2 targetV = normalizeVelocityVector(tBody.velocity);
      float tcd = agentController.getFlashCooldown() / agentController.flashCooldown;
      AddVectorObs (targetP);
      AddVectorObs (targetV);
      AddVectorObs (agentController.getIsVisible());
      AddVectorObs (tcd);
  //    Debug.Log("target data: " + targetP + "," + targetV + "," + agentController.getIsVisible() + "," + tcd );
      if(recordObservations) {
        targetsObservations[observationIndex] = new AgentProperties(targetP, targetV, agentController.getIsVisible(), tcd);
        observationIndex++;
      }
      definedTargets++;
    }
   // Debug.Log("-----------------------------");

    if(definedTargets < 3) {
      targetsObservations = fillRemaining(definedTargets, targetsObservations);
    }

    // Agent
    Vector2 agentP = normalizePositionVector(this.transform.position);
    Vector2 agentV = normalizeVelocityVector(controller.getBody().velocity);
    bool visibleF = controller.getIsVisible ();
    float pcd = controller.getFlashCooldown() / controller.flashCooldown;
    AddVectorObs (agentP);
    AddVectorObs (agentV);
    AddVectorObs (visibleF);
    AddVectorObs (pcd);
  //  Debug.Log("agent data: " + agentP + "," + agentV + "," + visibleF + "," + pcd );
    AgentProperties agentObservation;

    // Obstacle
    Vector2 ObstacleP = normalizePositionVector(controller.getLastCollision ());
    float hitTime = controller.getObstacleHitTimeStamp();
    AddVectorObs (ObstacleP);
    AddVectorObs (hitTime);
 //   Debug.Log("obstacle: " + ObstacleP + "," + hitTime);

    if(recordObservations) {
      agentObservation = new AgentProperties(agentP, agentV, visibleF, pcd);
      obsList.Add(new Observations(agentObservation, targetsObservations, ObstacleP, hitTime));
    }
  }

  public void ActionToInput (float[] vectorAction) {
    //Debug.Log("Vector action " + vectorAction + "id "+ controller.getPlayerId());
    if (vectorAction[2] == 1) {
      controller.Flash ();
    }

    if (vectorAction[3] == 1) {
      controller.Taunt ();
    }
    //Debug.Log("X:" + vectorAction[0] + "Y:" + vectorAction[0]);
    if (vectorAction[0] == 0 && vectorAction[1] == 0) {
      controller.ProcessMovement (Vector2.zero, 0);
      return;
    }
    controller.processHorizontalMovement (vectorAction);
    controller.processVerticalMovement (vectorAction);
    controller.processWesternMovement (vectorAction);
    controller.processEasternMovement (vectorAction);

  }

  void rewardForGettingNearTarget (float[] currentSeparations) {
    int index = 0;
    float[] targetSeparations = controller.getTargetSeparations (controller.getAlltargets ());
    foreach (float currentSeparation in currentSeparations) {
      if (currentSeparation < targetSeparations[index]) {
        AddReward (0.00002f);
      }
    }
  }

  public override void AgentAction (float[] vectorAction, string textAction) {
    ActionToInput (vectorAction);
    if(recordObservations) {
      if(obsList.Count > 0) {
        obsList[obsList.Count - 1].setVectorActions(vectorAction);
      }
    } 

    //Punish for taking too much time time
    AddReward (-0.00001f);

    //Punish for not moving 
    if (Vector2.Distance (controller.getLastPosition (), this.transform.position) < 0.05f) {
      AddReward (-0.00002f);
    }
    controller.setLastPosition (this.transform.position);
    float[] currentSeparation = controller.getTargetSeparations (controller.getAlltargets ());
    //Reward for getting close to targets
    rewardForGettingNearTarget (currentSeparation);
    controller.recordTargetSeparations (currentSeparation);

    // Captured a new target
    if (controller.getCapturedPlayersCounts () > controller.getLastCapturedPlayersLength ()) {
      SetReward (0.1f);
      controller.setLastCapturedPlayersLength (controller.getCapturedPlayersCounts ());
    }

    // Won game
    //Debug.Log("Count" + this.playerId + "is" + capturedPlayers.Count + "and" + maxTargets);
    if (controller.getCapturedPlayersCounts () == controller.getMaxTargets ()) {
      SetReward (0.3f);
      Done ();
      if(recordObservations) {
        Debug.Log("SAVED");
        gameObs = new GameObservation(obsList.ToArray());
        string result = GameObservation.stringify(gameObs);
        GameObservation.saveJson(result, "GameObservation.json");
      }
      academy.reset();
    }
  }
}