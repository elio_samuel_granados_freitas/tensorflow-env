﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Observations {
    public AgentProperties player = null;
    public AgentProperties[] targets = null;

    public float lastObstaclex;
    public float lastObstacley;
    public float ObstacleHitTimeStamp;

    public float horizontalMovement;
    public float verticalMovement;
    public float flash;

    public float taunt;

    public Observations (AgentProperties player, AgentProperties[] targets, Vector2 lastObstacle, float ObstacleHitTimeStamp) {
        this.player = player;
        this.targets = targets;
        lastObstaclex = lastObstacle.x;
        lastObstacley = lastObstacle.y;
        this.ObstacleHitTimeStamp = ObstacleHitTimeStamp;
    }

    public void setVectorActions (float[] vectorActions) {
        horizontalMovement = vectorActions[0];
        verticalMovement = vectorActions[1];
        flash = vectorActions[2];
        taunt = vectorActions[3];
    }

    public float[] getVectorActions () {
        return new float[4] { horizontalMovement, verticalMovement, flash, taunt };
    }

    public AgentProperties getPlayer () {
        return player;
    }

    public AgentProperties[] getTargets () {
        return targets;
    }

    public Vector2 getLastObstacle () {
        return new Vector2 (lastObstaclex, lastObstacley);
    }

    public float getObstacleHitTimeStamp () {
        return ObstacleHitTimeStamp;
    }
}