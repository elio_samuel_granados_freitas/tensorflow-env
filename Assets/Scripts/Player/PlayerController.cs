﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public GameObject[] targets;
    public Vector2 LastCollision;
    public bool isVisible;

    public float coneDuration;
    public float flashDuration;
    public float flashCooldown;
    public float tauntDuration;

    public Vector2 lastPosition;

    public float[] targetSeparations;
    public float speed = 10;

    private string playerId = null;

    public GameObject camRangeObj = null;
    public GameObject tauntObj = null;

    public GameObject _flashed = null;

    public List<string> capturedPlayers = new List<string> ();
    private int lastCapturedPlayersLength = 0;

    private Coroutine _flashRoutine;

    private Rigidbody2D _body;
    private float _lastFlashTime = -10f;

    //private int[] blockedHorizontalMovement = null;
    //private int[] blockedVerticalMovement = null;

    private int maxTargets = 0;

    private float lastHitCollisionTimeStamp;

    public string getPlayerId() {
        return this.playerId;
    }
    // Start is called before the first frame update
    void Start () {
        playerId = GetUniqueIdentifier ();
        _body = GetComponent<Rigidbody2D> ();
        lastCapturedPlayersLength = 0;
        targets = getTargets ();
        recordTargetSeparations (getTargetSeparations (targets));
        CameraRange CameraRange = camRangeObj.GetComponent<CameraRange> ();
        CameraRange.setParent (this);
        maxTargets = targets.Length;
        LastCollision = Vector2.zero;
    }

    public void reset () {
        capturedPlayers = new List<string> ();
        lastCapturedPlayersLength = 0;
        lastPosition = this.transform.position;
        _lastFlashTime = -10f;
        LastCollision = Vector2.zero;
        recordTargetSeparations (getTargetSeparations (targets));
        illuminate ();
        Invoke ("obscure", coneDuration);
    }

    public void ProcessMovement (Vector2 moveVector, float angle) {
        _body.velocity = moveVector * speed * Time.deltaTime;

        // Rotate object with left stick or d-pad.
        if (Mathf.Abs (moveVector.x) > Mathf.Epsilon || Mathf.Abs (moveVector.y) > Mathf.Epsilon) {
            _body.velocity += new Vector2 (speed * Time.deltaTime * moveVector.x, 0);
            _body.velocity += new Vector2 (0, speed * Time.deltaTime * moveVector.y);
        }
    }

    public void processHorizontalMovement (float[] vectorAction) {
        if (vectorAction[0] == 1 && vectorAction[1] == 0) {
            //Debug.Log("right");
            ProcessMovement (new Vector2 (1.0f, 0.0f), 90);
            camRangeObj.transform.eulerAngles = new Vector3 (0, 0, 0);
        } else if (vectorAction[0] == 2 && vectorAction[1] == 0) {
            //Debug.Log("left");
            ProcessMovement (new Vector2 (-1.0f, 0.0f), 270);
            camRangeObj.transform.eulerAngles = new Vector3 (0, 0, 180);
        }
    }

    public void processVerticalMovement (float[] vectorAction) {
        if (vectorAction[1] == 1 && vectorAction[0] == 0) {
            ProcessMovement (new Vector2 (0.0f, 1.0f), 0);
            camRangeObj.transform.eulerAngles = new Vector3 (0, 0, 90);
        } else if (vectorAction[1] == 2 && vectorAction[0] == 0) {
            ProcessMovement (new Vector2 (0.0f, -1.0f), 180);
            camRangeObj.transform.eulerAngles = new Vector3 (0, 0, 270);
        }
    }

    public void processWesternMovement (float[] vectorAction) {
        if (vectorAction[1] == 1 && vectorAction[0] == 2) {
            ProcessMovement (new Vector2 (-1.0f, 1.0f), 135);
            camRangeObj.transform.eulerAngles = new Vector3 (0, 0, 135);
        } else if (vectorAction[1] == 2 && vectorAction[0] == 2) {
            ProcessMovement (new Vector2 (-1.0f, -1.0f), 225);
            camRangeObj.transform.eulerAngles = new Vector3 (0, 0, 225);
        }
    }

    public void processEasternMovement (float[] vectorAction) {
        if (vectorAction[1] == 1 && vectorAction[0] == 1) {
            ProcessMovement (new Vector2 (1.0f, 1.0f), 45);
            camRangeObj.transform.eulerAngles = new Vector3 (0, 0, 45);
        } else if (vectorAction[1] == 2 && vectorAction[0] == 1) {
            ProcessMovement (new Vector2 (1.0f, -1.0f), 315);
            camRangeObj.transform.eulerAngles = new Vector3 (0, 0, 315);
        }
    }

    public bool CaughtPlayerOnPhoto (PlayerController CaughtPlayer) {
        if (!capturedPlayers.Contains (CaughtPlayer.playerId)) {
            capturedPlayers.Add (CaughtPlayer.playerId);
            return true;
        }
        return false;
    }

    private void OnCollisionEnter2D (Collision2D other) {
        LastCollision = other.transform.position;
        lastHitCollisionTimeStamp = Time.time;

        //var relativePosition = transform.InverseTransformPoint(other.transform.position);

       /*if(relativePosition.x > 0) 
        {
          blockedHorizontalMovement = new int[1] {1}
        } 
        else 
        {
          Debug.Log("Object is west");
        }

        if(relativePosition.y > 0) 
        {
          Debug.Log("Object is north");
        } 
        else 
        {
          Debug.Log("Object is south");
        }*/
        illuminate ();
        Invoke ("obscure", coneDuration);
    }

    private GameObject[] getTargets () {
        GameObject[] players = GameObject.FindGameObjectsWithTag ("Player");
        GameObject[] onlyTargets = new GameObject[players.Length - 1];
        int playerIndex = 0;
        int targetIndex = 0;
        while (targetIndex < onlyTargets.Length) {
            PlayerController controller = players[playerIndex].GetComponent<PlayerController>(); 
            if (controller.playerId != this.playerId) {
                onlyTargets[targetIndex] = players[playerIndex];
                targetIndex++;
            }
            playerIndex++;
        }
        return onlyTargets;
    }

    public static string GetUniqueIdentifier () {
        System.Guid uid = System.Guid.NewGuid ();
        return uid.ToString ();
    }

    public void Flash () {
        //if (IsInvoking("EndFlash"))
        //    return;

        if (Time.time > _lastFlashTime + flashCooldown) {
            _lastFlashTime = Time.time;

            camRangeObj.SetActive (true);
            illuminate ();
            //onDidFlash.Invoke();
            //GameManager.Instance.PlayerTookPhoto(GetPlayerID());
            Invoke ("EndConeSprite", coneDuration);
            Invoke ("obscure", coneDuration);
            SetupEndFlash ();
        }
    }

    void EndConeSprite () {
        camRangeObj.SetActive (false);
    }

    void illuminate () {
        isVisible = true;
        tauntObj.SetActive (true);
    }

    void obscure () {
        isVisible = false;
        tauntObj.SetActive (false);
    }

    public void SetupEndFlash () {
        Invoke ("EndFlash", flashDuration);
    }

    void EndFlash () {
        //areaLightObj.SetActive(false);
        //lightConeObj.SetActive(false);
    }

    public void WasFlashed () {
        if (_flashRoutine != null)
            StopCoroutine (_flashRoutine);
        _flashRoutine = StartCoroutine (OnPlayerFlashed ());
    }

    IEnumerator OnPlayerFlashed () {
        _flashed.gameObject.SetActive (true);

        yield return new WaitForSeconds (flashDuration / 2);

        EndFlashed ();
    }

    void EndFlashed () {
        _flashed.gameObject.SetActive (false);
    }

    public void Taunt () {
        if (IsInvoking ("DisableTaunt"))
            return;
        tauntObj.SetActive (true);
        isVisible = false;
        Invoke ("DisableTaunt", tauntDuration);
    }

    void DisableTaunt () {
        isVisible = true;
        tauntObj.SetActive (false);
    }

    public GameObject[] getAlltargets () {
        return targets;
    }

    public float getFlashCooldown() {
        float remaining = flashCooldown - (Time.time - _lastFlashTime);
        return remaining < 0 ? 0 : remaining;
    }

   public float getObstacleHitTimeStamp () {
        return lastHitCollisionTimeStamp;
    }

    public Vector2 getLastCollision () {
        return LastCollision;
    }

    public bool getIsVisible () {
        return isVisible;
    }

    public Rigidbody2D getBody () {
        return _body;
    }

    public void recordTargetSeparations (float[] separations) {
        targetSeparations = separations;
    }

    public float[] getTargetSeparations (GameObject[] targets) {
        float[] separations = new float[targets.Length];
        int index = 0;
        foreach (float separation in separations) {
            float distance = Vector2.Distance (targets[index].transform.position, this.transform.position);
            if (distance > 0) {
                separations[index] = distance;
            }
            index++;
        }
        return separations;
    }

    public void setLastPosition (Vector2 position) {
        lastPosition = position;
    }

    public Vector2 getLastPosition () {
        return lastPosition;
    }

    public int getCapturedPlayersCounts () {
        return capturedPlayers.Count;
    }

    public int getLastCapturedPlayersLength () {
        return lastCapturedPlayersLength;
    }

    public void setLastCapturedPlayersLength (int Length) {
        lastCapturedPlayersLength = Length;
    }

    public int getMaxTargets () {
        return maxTargets;
    }

}