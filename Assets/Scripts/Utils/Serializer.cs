﻿using System;
using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

public class Serializer
{
    public static string userDataPath { get { return Application.persistentDataPath + "/"; } }

    public static T Load<T>(string filename) where T : class
    {
        string path = Application.persistentDataPath + "/" + filename;
        return LoadFullPath<T>(path);
    }


    public static T LoadFullPath<T>(string path) where T : class
    {
        //Debug.Log("Trying to load " + path);

        if (File.Exists(path))
        {
            try
            {
                using (Stream stream = File.OpenRead(path))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    return formatter.Deserialize(stream) as T;
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }
        return default(T);
    }

    public static void Save<T>(string filename, T data) where T : class
    {
        string path = Application.persistentDataPath + "/" + filename;
        Debug.Log("Saving to " + path);
        using (Stream stream = File.OpenWrite(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, data);
        }
    }

    public static void ClearAllContentWithExtension(string fileExtension) {
        string[] filePaths = Directory.GetFiles(userDataPath);
        foreach (string filePath in filePaths) {
            Debug.Log("Checking file " + filePath + " for extension " + fileExtension);
            bool hasExtension = filePath.Contains(fileExtension);
            if (hasExtension) {
                Debug.Log("Deleting " + filePath);
                File.Delete(filePath);
            }
                
        }
            
    }
}