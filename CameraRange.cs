﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRange : MonoBehaviour {

	private PlayerAgent _parent;

	// Use this for initialization
	void Start () {
		_parent = this.transform.parent.GetComponent<PlayerAgent> ();
	}

	void OnTriggerEnter2D(Collider2D other) {
		PlayerAgent caughtPlayer = other.GetComponent<PlayerAgent> ();
		if (caughtPlayer != null && caughtPlayer != _parent) {
			_parent.CaughtPlayerOnPhoto (caughtPlayer);
		}
	}
}
