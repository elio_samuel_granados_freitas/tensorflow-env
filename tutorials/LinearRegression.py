import numpy as np
import tensorflow as tf
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
# === Create data and simulate results =====
x_data = np.random.randn(2000,3) #create 2000 points from values 0 to 3 and -3
w_real = [0.3,0.5,0.1]
b_real = -0.2
noise = np.random.randn(1,2000)*0.1 # gaussian noise
y_data = np.matmul(w_real,x_data.T) + b_real + noise

NUM_STEPS = 10
g = tf.Graph()
wb_ = []
with g.as_default():
    x = tf.placeholder(tf.float32,shape=[None,3])#We define the x values placeholders x=data
    y_true = tf.placeholder(tf.float32,shape=None)#We define the Y values placeholders this is equivalent to y=data

    with tf.name_scope('inference') as scope:
        w = tf.Variable([[0,0,0]],dtype=tf.float32,name='weights')#initial value = [[0,0,0]], dtype is the type conversion of the variable, name of the variable
        b = tf.Variable(0,dtype=tf.float32,name='bias')
        y_pred = tf.matmul(w,tf.transpose(x)) + b #we multiply the unknown weights by the noisy the exact same multiplication we did before just we random weights
    
    with tf.name_scope('loss') as scope:# we calculate de difference between both that should be big initially
        loss = tf.reduce_mean(tf.square(y_true-y_pred))

    with tf.name_scope('train') as scope:
        learning_rate = 0.5
        optimizer = tf.train.GradientDescentOptimizer(learning_rate)
        train = optimizer.minimize(loss)#this automatically updates the variables that have the GraphKyes.TRAINABLE_VARIABLES, which should be it's third parameter

    # Before starting, initialize the variables. We will 'run' this first.
    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)
        for step in range(NUM_STEPS):
            sess.run(train,{x: x_data, y_true: y_data})#fetches(the value that it will remenber, like a sort of setter) the train
            if (step % 5 == 0):
                print(step, sess.run([w,b]))
                wb_.append(sess.run([w,b]))

        print(10, sess.run([w,b]))