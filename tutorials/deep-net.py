import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
from tensorflow.contrib import rnn

# Input > weight > hidden layer 1 (activation function) > weights > hidden l2  > activation function > output layer
# compare output to intended output with -> cost/ loss function
# Optimization function (optimizer) > minimize cost by changing weights

# feed forward + backprop = epoch

mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)

#784 = 28x28
flatten_pixels_dimensions = 784

hm_epochs = 10 # who many runs 
#numbers of types 1 to 10
n_classes = 10

#how much data to take in one at a time
batch_size = 128

#images is 28 x 28 so we are going to have 28 groups of 28 bits
chunk_size = 28
n_chunks = 28

rnn_size = 128


#matrix will be height x width
x = tf.compat.v1.placeholder('float', shape=[None, n_chunks, chunk_size])
y = tf.compat.v1.placeholder('float')

def recurrent_neural_network(x):
    #(input_data * weights) + biases ; biases avoid cases where input data is blank = 0 because neurons wont fire
    layer = {
        'weights': tf.Variable(tf.random.normal([rnn_size, n_classes])),
        'biases': tf.Variable(tf.random.normal([n_classes]))
    }

    x = tf.transpose(x, [1,0,2])
    x = tf.reshape(x, [-1, chunk_size])
    x = tf.split(x, n_chunks, 0)

    lstm_cell = tf.keras.layers.LSTMCell(rnn_size) 
    outputs, states = tf.keras.layers.RNN(lstm_cell, x, dtype=tf.float32, return_sequences=True, return_state=True)
    output = tf.matmul(outputs[-1],layer['weights']) + layer['biases']

    return output

def train_neural_network(x):
    predicted_y = recurrent_neural_network(x)
    # NEW:
    cost = tf.reduce_mean( tf.nn.softmax_cross_entropy_with_logits(logits=predicted_y, labels=y) ) # determines the differences
    optimizer = tf.train.AdamOptimizer().minimize(cost) # trains the nn modifying the weights

    with tf.Session() as sess:
        # NEW:
        sess.run(tf.global_variables_initializer())

        for epoch in range(hm_epochs):
            epoch_loss = 0
            for _ in range(int(mnist.train.num_examples/batch_size)): #total mnist samples / group size
                epoch_x, epoch_y = mnist.train.next_batch(batch_size)
                epoch_x = epoch_x.reshape((batch_size, n_chunks, chunk_size))
                _, c = sess.run([optimizer, cost], feed_dict={x: epoch_x, y: epoch_y})
                epoch_loss += c

            print('Epoch', epoch, 'completed out of',hm_epochs,'loss:',epoch_loss)
        
        correct = tf.equal(tf.argmax(predicted_y, 1), tf.argmax(y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct, 'float'))
        print('Accuracy:',accuracy.eval({x:mnist.test.images.reshape((-1, n_chunks, chunk_size)), y:mnist.test.labels}))

train_neural_network(x)
